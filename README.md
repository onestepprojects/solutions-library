# Module 8: Solutions Library Module
Solutions Library Module for the OneStep Relief Platform

## Project Information

* [Introduction](#introduction)
* [Team Members](#team-members)
* [Product Owners](#product-ownsers)
* [Faculty](#faculty)
* [Technical Details](#technical-details)
* [Development Tools](#development-tools)


### Introduction

Solutions Library
### Team Members

* Eric Ding
* Eric S Moon
* Gustavo Varo
* Madhan Manoharan
* Michaela DeForest
* Oscar Anchondo

### Product Owners

* Nitya Timalsina
* Vijay Bhatt

### Faculty
* Annie Kamlang
* Eric Gieseke
* Hannah Riggs

### Agile Coach
* Hannah Riggs

### Technical Details



### Development Tools


* [Requirements Document](https://docs.google.com/document/d/1KGzL5NS6QMWYavRQqC0MRIb8fJpR7lrxagt9yqWopjU/edit)
* [OneStep Relief Platform System Architecture](https://docs.google.com/document/d/1jLGySl6OOGIB6STxu-LDSxUQ1Z6pus0Vp5dqvp4tyLY/edit#heading=h.odpaxmoutbnv)
* [Design Document](https://docs.google.com/document/d/1Hsp-nK54ij0bKaujdCu2fI-XV2unWaXtx9Lps_8dHjc/edit)

* Tech review and research
    * [Dropwizard](https://www.dropwizard.io/en/latest/)
    * [DynamoDB](https://aws.amazon.com/dynamodb/?trk=ps_a134p000004f2afAAA&trkCampaign=acq_paid_search_brand&sc_channel=PS&sc_campaign=acquisition_US&sc_publisher=Google&sc_category=Database&sc_country=US&sc_geo=NAMER&sc_outcome=acq&sc_detail=dynamodb&sc_content=DynamoDB_e&sc_matchtype=e&sc_segment=468100518402&sc_medium=ACQ-P|PS-GO|Brand|Desktop|SU|Database|DynamoDB|US|EN|Text&s_kwcid=AL!4422!3!468100518402!e!!g!!dynamodb&ef_id=Cj0KCQiA4feBBhC9ARIsABp_nbVi9dybIMJGHn0K8C7gtSShfuF-G0JInpEegLtWigvTDdVui28TOfcaAilwEALw_wcB:G:s&s_kwcid=AL!4422!3!468100518402!e!!g!!dynamodb)
    * [Balsamiq](https://balsamiq.com/)

## UI Development

The frontend portion of this project is contained within the `frontend/` directory. The project is set up as an npm module that can be packaged and pulled in by the OneStep UI Application. This module is also capable of functioning on it's own, in an effort to encourage modularity.

To begin developing the UI, you must have node installed and at least two terminal windows open.

1. The first terminal window will allow you to make changes to the project (under `frontend/src`) and have them reload in the browser window after they are packaged into `frontend/dist/`.
Run `npm run start` from the `frontend/` directory and keep this running.

2. The second terminal window will be used for running the App server for the example app (under `frontend/example`). This "example" app pulls in our health module from the generated files as an npm package (see `frontend/example/App.js`), so that you can see the module in the browser. This example app will not be deployed with the module.
Run `npm run start` from the `frontend/example` directory and keep this running as well.

All UI work should be done in the `frontend/src` directory. NPM packages can be installed from `frontend/` and used in the module.

Components are organized in the `frontend/src/components` directory by page and tests are located under `frontend/src/tests`. The Module is exported from `frontend/src/index/js` and this is the starting point.




