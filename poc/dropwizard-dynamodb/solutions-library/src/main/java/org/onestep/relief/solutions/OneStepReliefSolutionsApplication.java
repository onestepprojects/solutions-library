package org.onestep.relief.solutions;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.onestep.relief.solutions.core.Solution;
import org.onestep.relief.solutions.db.BlogDAO;
import org.onestep.relief.solutions.db.SolutionDAO;
import org.onestep.relief.solutions.resources.BlogResource;
import org.onestep.relief.solutions.resources.SolutionResource;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Map;

public class OneStepReliefSolutionsApplication extends Application<OneStepReliefSolutionsConfiguration> {

    public static void main(final String[] args) throws Exception {
        new OneStepReliefSolutionsApplication().run(args);
    }

    private final HibernateBundle<OneStepReliefSolutionsConfiguration> hibernateBundle =
            new HibernateBundle<OneStepReliefSolutionsConfiguration>(Solution.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(OneStepReliefSolutionsConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public String getName() {
        return "OneStepReliefSolutions";
    }

    @Override
    public void initialize(final Bootstrap<OneStepReliefSolutionsConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
        bootstrap.addBundle(new MigrationsBundle<OneStepReliefSolutionsConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(OneStepReliefSolutionsConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(final OneStepReliefSolutionsConfiguration configuration,
                    final Environment environment) {

        // Enable CORS headers
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final SolutionDAO solutionDAO = new SolutionDAO(hibernateBundle.getSessionFactory());
        environment.jersey().register(new SolutionResource(solutionDAO));

        final BlogDAO blogDAO = new BlogDAO();
        environment.jersey().register(new BlogResource(blogDAO));
    }

}
