package org.onestep.relief.solutions.db;

import io.dropwizard.hibernate.AbstractDAO;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.onestep.relief.solutions.core.Solution;

import java.util.List;
import java.util.Optional;

public class SolutionDAO extends AbstractDAO<Solution> {
    public SolutionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Solution> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Solution create(Solution solution) {
        return persist(solution);
    }

    @SuppressWarnings("unchecked")
    public List<Solution> findAll() {
        return list((Query<Solution>) namedQuery("org.onestep.relief.solutions.core.findAll"));
    }
}
