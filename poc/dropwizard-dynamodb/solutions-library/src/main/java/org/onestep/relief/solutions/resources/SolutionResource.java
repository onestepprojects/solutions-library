package org.onestep.relief.solutions.resources;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import org.onestep.relief.solutions.core.Solution;
import org.onestep.relief.solutions.db.SolutionDAO;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/solution")
@Produces(MediaType.APPLICATION_JSON)
public class SolutionResource {
    private final SolutionDAO solutionDAO;

    public SolutionResource(SolutionDAO projectDAO) {
        this.solutionDAO = projectDAO;
    }


    @GET
    @Path("/{solutionId}")
    @UnitOfWork
    public Solution getProject(@PathParam("solutionId") LongParam solutionId) {
        return findSafely(solutionId.get());
    }

    @GET
    @UnitOfWork
    public List<Solution> listProject() {
        return solutionDAO.findAll();
    }

    @POST
    @UnitOfWork
    public Solution createProject(@Valid Solution solution) {
        return solutionDAO.create(solution);
    }

    private Solution findSafely(long solutionId) {
        return solutionDAO.findById(solutionId).orElseThrow(() -> new NotFoundException("No such Solution."));
    }
}
