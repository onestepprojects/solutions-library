package org.onestep.relief.solutions.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

@Entity
@Table(name = "solution")
@NamedQueries(
        {
                @NamedQuery(
                        name = "org.onestep.relief.solutions.core.findAll",
                        query = "SELECT s FROM Solution s"
                )
        })
public class Solution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    public Solution() {
    }

    public Solution(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Solution)) {
            return false;
        }

        Solution solutionOther = (Solution) o;

        return id == solutionOther.id &&
                Objects.equals(name, solutionOther.name) &&
                Objects.equals(description, solutionOther.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
