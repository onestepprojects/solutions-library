package org.onestep.relief.solutions.resources;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.jersey.params.NonEmptyStringParam;
import org.onestep.relief.solutions.core.Blog;
import org.onestep.relief.solutions.core.Solution;
import org.onestep.relief.solutions.db.BlogDAO;
import org.onestep.relief.solutions.db.SolutionDAO;

import javax.ws.rs.Path;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/blog")
@Produces(MediaType.APPLICATION_JSON)
public class BlogResource {

    private BlogDAO blogDAO;

    public BlogResource(BlogDAO blogDAO) {
        this.blogDAO = blogDAO;
    }


    @GET
    @Path("/{blogId}")
    @UnitOfWork
    public Blog getBlog(@PathParam("blogId") String blogId) {
        return findSafely(blogId);
    }

    @GET
    @UnitOfWork
    public List<Blog> listBlogs() {
        return this.blogDAO.getBlogs();
    }

    @POST
    @UnitOfWork
    public Blog creatBlog(@Valid Blog blog) {
        this.blogDAO.saveBlog(blog);
        return blog;
    }

    private Blog findSafely(String blogId) {
        try {
            return this.blogDAO.getBlog(blogId);
        }
        catch (Exception e) {
            throw new NotFoundException("No such Blog. " + e.getMessage());
        }
    }

}
