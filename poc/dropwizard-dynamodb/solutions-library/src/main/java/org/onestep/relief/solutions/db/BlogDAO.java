package org.onestep.relief.solutions.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.onestep.relief.solutions.core.Blog;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

public class BlogDAO {

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;
    private DynamoDBMapperConfig dynamoDBMapperConfig;

    @Inject
    public BlogDAO () {
        this.client = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        this.dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement("blog")
                )
                .build();
        this.mapper = new DynamoDBMapper(client, this.dynamoDBMapperConfig);
    }

    public List<Blog> getBlogs() {
        return this.mapper.scan(Blog.class, new DynamoDBScanExpression());
    }

    public Blog getBlog(String id) {
        return this.mapper.load(Blog.class, id);
    }

    public void saveBlog(Blog blog) {
        if(blog.getId() == null) {
            blog.setId(UUID.randomUUID().toString());
        }
        this.mapper.save(blog);
    }

    public void deleteProject(String id) {
        Blog blog = this.getBlog(id);
        if(blog != null) {
            this.mapper.delete(blog);
        }
    }

}
