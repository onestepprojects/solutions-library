# OneStepReliefSolutions

This is a demo to showcase Dropwizard REST API.

The 'Solution' model uses a Hibernate DB.
The 'Blog' model uses a Dynamo DB.

How to create a Dropwizard project
---
```
mvn archetype:generate -DarchetypeGroupId=io.dropwizard.archetypes -DarchetypeArtifactId=java-simple -DarchetypeVersion=2.0.20 -DgroupId=org.onestep.relief.solutions -DartifactId=solutions-library -Dversion=1.0.0-SNAPSHOT -Dname=OneStepReliefSolutions
```

How to configure AWS
---
[AWS CLI Configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

How to start the OneStepReliefSolutions application
---

1. Run `mvn package` to build and package your application
1. Setup Hibernate DB using `java -jar target/solutions-library-1.0.0-SNAPSHOT.jar db migrate config.yml`
1. Start application using `java -jar target/solutions-library-1.0.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`
1. To check 'Solution' resource, navigate to `http://localhost:8080/solution`
1. To check 'Blog' resource `http://localhost:8080/blog`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
