import React, { Component } from "react";

class Projects extends Component {
  constructor(props) {
    super(props);

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);

    this.state = {
      projectName: "",
      nodes: null,
    };
  }

  async componentDidMount() {}

  async handleClick(projectName, e) {
    e.preventDefault();

    let projectsUrl =
      process.env.REACT_APP_BASE_URL + "/" + projectName + "/nodes";
    fetch(projectsUrl, {
      headers: {
        Authorization: "Bearer " + this.props.idToken,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({ nodes: JSON.stringify(data.data) });
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <center>
          <h1>Gentics Mesh Projects</h1>
        </center>
        {this.props.projects.map((project, i) => (
          <div key={i} className="card">
            <div key={i} className="card-body">
              <h5 key={i} className="card-title">
                {project.name}
              </h5>
              <h6 key={i + 1} className="card-subtitle mb-2 text-muted">
                uuid: {project.uuid}
              </h6>
              <p key={i + 2} className="card-text">
                Creator: {project.creator.uuid}
              </p>
              <p key={i + 3} className="card-text">
                Created on: {new Date(project.created).toString()}
              </p>
              <p key={i + 4} className="card-text">
                Last edited: {new Date(project.edited).toString()}
              </p>
              <button onClick={(e) => this.handleClick(project.name, e)}>
                Nodes
              </button>
              <div className="box-container">{this.state.nodes}</div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default Projects;
