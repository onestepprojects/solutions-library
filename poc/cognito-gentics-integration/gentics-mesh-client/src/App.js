import React, { Component, useContext, useState } from "react";
import Amplify, { Auth } from "aws-amplify";
import logo from "./logo.svg";
import Projects from "./components/projects";
import "./App.css";
import { withAuthenticator, AmplifySignOut } from "@aws-amplify/ui-react";

Amplify.configure({
  Auth: {
    region: process.env.REACT_APP_REGION,
    userPoolId: process.env.REACT_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID,
  },
});

class App extends Component {
  state = {
    projects: [],
    idToken: "",
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Gentics Mesh</h1>
          <h2>Authentication for Create React App using AWS Cognito</h2>
        </header>
        <Projects projects={this.state.projects} idToken={this.state.idToken} />
        <AmplifySignOut />
      </div>
    );
  }

  async componentDidMount() {
    let idToken;
    Auth.currentSession()
      .then((data) => {
        idToken = data.idToken;
        this.setState({ idToken: idToken.jwtToken });
      })
      .then(() => {
        let projectsUrl = process.env.REACT_APP_BASE_URL + "/projects";
        fetch(projectsUrl, {
          headers: {
            Authorization: "Bearer " + this.state.idToken,
          },
        })
          .then((response) => response.json())
          .then((data) => {
            this.setState({ projects: data.data });
          })
          .catch(console.log);
      });
  }
}

export default withAuthenticator(App);
