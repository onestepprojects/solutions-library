import { defineFileAction, ChonkyIconName } from 'chonky'

export const viewSolutionDiscussionBoard = defineFileAction({
  id: 'view_solution_discussion_board',
  requiresSelection: true,
  fileFilter: (file) => !file.isDir,
  button: {
    name: 'View Discussion Board',
    contextMenu: true,
    toolbar: true
  }
})

export const addNewSubcategory = defineFileAction({
  id: 'add_subcategory',
  button: {
    name: 'Add Subcategory',
    toolbar: true,
    tooltip: 'Create a folder',
    icon: ChonkyIconName.folderCreate
  }
})

export const approve = defineFileAction({
  id: 'approve_solution',
  requiresSelection: true,
  button: {
    name: 'Approve Solution',
    contextMenu: true,
    toolbar: true
  }
})
