import React, { useState } from 'react'
import Autosuggest from 'react-autosuggest'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios'

import theme from './AutosuggestTheme.module.css'
import styles from './search.module.css'

const Search = ({ placeholder, requestUrl, inputData, onDataSelected }) => {
  const [value, setValue] = useState('')
  const [data, setData] = useState([])

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const getSuggestionsAsync = (value) => {
    async function gatherData(value) {
      const cancelToken = axios.CancelToken
      let source = cancelToken.source()

      source && source.cancel('Operation canceled due to new request')

      source = axios.CancelToken.source()

      try {
        const result = await axios.get(requestUrl + '&terms=' + value, {
          cancelToken: source.token
        })

        return result.data[1]
      } catch (err) {
        //
      }
    }

    gatherData(value).then((result) => setData(result))
  }

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase()
    const inputLength = inputValue.length

    return inputLength === 0
      ? inputData
      : inputData.filter(
          (datum) =>
            datum.name.toLowerCase().slice(0, inputLength) === inputValue
        )
  }

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  const getSuggestionValue = (suggestion) => suggestion

  const renderSuggestion = (suggestion) => <span>{suggestion.name}</span>

  const onChange = (event, { newValue }) => {
    setValue(newValue)
  }

  const onSuggestionsFetchRequested = ({ value }) => {
    if (inputData.length !== 0) {
      setData(getSuggestions(value))
    } else {
      getSuggestionsAsync(value)
    }
  }

  const onSuggestionsClearRequested = () => {
    setData([])
  }

  const onSuggestionSelected = (event, { suggestionIndex }) => {
    onDataSelected({ suggestionIndex: suggestionIndex, allSuggestions: data })
  }

  const shouldRenderSuggestions = (value, reason) => {
    if (requestUrl !== '') {
      return true
    }

    if (reason === 'input-focused') {
      return true
    }

    return false
  }

  const renderInputComponent = (inputProps) => (
    <React.Fragment>
      <FontAwesomeIcon icon={faSearch} className={styles.searchIcon} />
      <input {...inputProps} />
    </React.Fragment>
  )

  const inputProps = {
    placeholder: placeholder,
    value,
    onChange: onChange,
    disabled: data.length === 0 && inputData.length === 0
  }

  return (
    <React.Fragment>
      <Autosuggest
        suggestions={data}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        onSuggestionSelected={onSuggestionSelected}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        renderInputComponent={renderInputComponent}
        shouldRenderSuggestions={shouldRenderSuggestions}
        inputProps={inputProps}
        theme={theme}
      />
    </React.Fragment>
  )
}

Search.defaultProps = {
  inputData: [],
  onDataSelected: () => {}
}

export default Search
