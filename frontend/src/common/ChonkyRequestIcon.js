import React from 'react'

import { ChonkyIconFA } from 'chonky-icon-fontawesome'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestion } from '@fortawesome/free-solid-svg-icons'

export const ChonkyRequestIcon = React.memo((props) => {
  if (props.icon === 'requestIcon') {
    return <FontAwesomeIcon icon={faQuestion} size='sm' fixedWidth />
  }

  return <ChonkyIconFA {...props} />
})

ChonkyRequestIcon.displayName = 'ChonkyRequestIcon'
