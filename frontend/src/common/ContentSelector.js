import React, { useState } from 'react'

import { Link } from 'react-router-dom-v5'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPlus,
  faPaperclip,
  faLink,
  faFileAlt,
  faTimes
} from '@fortawesome/free-solid-svg-icons'
import { Modal, Button } from 'react-bootstrap'

const ContentSelector = ({
  files,
  setFiles,
  links,
  setLinks,
  text,
  setText
}) => {
  const [show, setShow] = useState(false)
  const [showText, setShowText] = useState(null)
  const [curType, setCurType] = useState('file')

  const [curFile, setCurFile] = useState({})
  const [curLinkText, setCurLinkText] = useState('')
  const [curLink, setCurLink] = useState('')
  const [curText, setCurText] = useState('')

  const uploadTypes = {
    file: {
      title: 'Upload File',
      content: (
        <input
          type='file'
          onChange={(e) =>
            setCurFile({
              file: e.target.files[0],
              fileName: e.target.files[0].name
            })
          }
          className='form-control-file'
        />
      )
    },
    link: {
      title: 'Attach Link',
      content: (
        <React.Fragment>
          <div className='form-group'>
            <label htmlFor='uploadLinkText'>Text</label>
            <input
              type='text'
              className='form-control'
              id='uploadLinkText'
              onChange={(e) => setCurLinkText(e.target.value)}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='uploadLink'>Link</label>
            <input
              type='text'
              className='form-control'
              id='uploadLink'
              onChange={(e) => setCurLink(e.target.value)}
            ></input>
          </div>
        </React.Fragment>
      )
    },
    text: {
      title: 'Attach Text',
      content: (
        <div className='form-group'>
          <label htmlFor='uploadText'>Text</label>
          <textarea
            type='text'
            className='form-control'
            rows={5}
            id='uploadText'
            onChange={(e) => setCurText(e.target.value)}
          ></textarea>
        </div>
      )
    }
  }

  const handleClose = (buttonType) => {
    if (buttonType === 'submit') {
      if (curFile.fileName) {
        setFiles([...files, curFile])
        setCurFile({})
      } else if (curLink !== '') {
        setLinks([...links, { placeholder: curLinkText, link: curLink }])
        setCurLinkText('')
        setCurLink('')
      } else if (curText !== '') {
        setText([...text, curText])
        setCurText('')
      }
    }

    setShow(false)
  }

  const handleShow = (uploadType) => {
    setCurType(uploadType)
    setShow(true)
  }

  const removeAttachment = (attachmentType, index) => {
    if (attachmentType === 'file') {
      setFiles(files.filter((_file, fileIdx) => fileIdx !== index))
    } else if (attachmentType === 'link') {
      setLinks(links.filter((_link, linkIdx) => linkIdx !== index))
    } else if (attachmentType === 'text') {
      setText(text.filter((_item, itemIdx) => itemIdx !== index))
    }
  }

  const displayAttachmentInput = () => {
    const uploadObj = uploadTypes[curType]

    return (
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>{uploadObj.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{uploadObj.content}</Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => handleClose('close')}>
            Close
          </Button>
          <Button variant='primary' onClick={() => handleClose('submit')}>
            Upload
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }

  const displayText = () => {
    return (
      <Modal show={showText} onHide={() => setShowText(null)}>
        <Modal.Header>
          <Modal.Title>{showText && showText.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{showText && showText.text}</Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => setShowText(null)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }

  return (
    <React.Fragment>
      <div className='mt-4' role='group'>
        <button
          type='button'
          className='btn btn-primary mr-3'
          onClick={() => handleShow('file')}
        >
          <FontAwesomeIcon className='mr-2' size='lg' icon={faPlus} />
          Upload File
        </button>
        <button
          type='button'
          className='btn btn-primary mr-3'
          onClick={() => handleShow('link')}
        >
          <FontAwesomeIcon className='mr-2' size='lg' icon={faPlus} />
          Add Link
        </button>
        <button
          type='button'
          className='btn btn-primary'
          onClick={() => handleShow('text')}
        >
          <FontAwesomeIcon className='mr-2' size='lg' icon={faPlus} />
          Add Text
        </button>
      </div>
      <div className='mt-4'>
        {files.map((file, index) => (
          <div key={index}>
            <div className='row'>
              <div className='col'>
                <FontAwesomeIcon className='mr-2' icon={faPaperclip} />
                <a
                  href={
                    file.file && file.fileName
                      ? URL.createObjectURL(
                          new Blob([file.file], { type: file.file.type })
                        )
                      : `${process.env.REACT_APP_API_URL}/api/v2/solutions/nodes/${file.uuid}/binary/binary`
                  }
                  download
                >
                  {file.fileName}
                </a>
              </div>
              <div className='col'>
                <FontAwesomeIcon
                  color='red'
                  icon={faTimes}
                  className='link'
                  onClick={() => {
                    removeAttachment('file', index)
                  }}
                />
              </div>
            </div>
          </div>
        ))}
        {links.map((link, index) => (
          <div key={index}>
            <div className='row'>
              <div className='col'>
                <FontAwesomeIcon className='mr-2' icon={faLink} />
                <a href={link.link} target='blank'>
                  {link.placeholder}
                </a>
              </div>
              <div className='col'>
                <FontAwesomeIcon
                  color='red'
                  icon={faTimes}
                  className='link'
                  onClick={() => {
                    removeAttachment('link', index)
                  }}
                />
              </div>
            </div>
          </div>
        ))}
        {text.map((item, index) => (
          <div key={index}>
            <div className='row'>
              <div className='col'>
                <FontAwesomeIcon className='mr-2' icon={faFileAlt} />
                <Link
                  onClick={() =>
                    setShowText({ title: `text_${index + 1}`, text: item })
                  }
                >
                  text_{index + 1}
                </Link>
              </div>
              <div className='col'>
                <FontAwesomeIcon
                  color='red'
                  icon={faTimes}
                  className='link'
                  onClick={() => {
                    removeAttachment('text', index)
                  }}
                />
              </div>
            </div>
          </div>
        ))}
      </div>
      {displayAttachmentInput()}
      {displayText()}
    </React.Fragment>
  )
}

export default ContentSelector
