/* eslint-disable no-undef */
import React from 'react'

import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import { FileBrowser } from 'chonky'

import Pending from '../components/Pending'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Categories', () => {
  const routeComponentPropsMock = {
    history: {},
    location: {},
    match: {}
  }

  const wrapper = shallow(<Pending {...routeComponentPropsMock} />)

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render a file browser component', () => {
    expect(wrapper.containsMatchingElement(<FileBrowser />))
  })
})
