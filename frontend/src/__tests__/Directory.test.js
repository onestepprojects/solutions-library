/* eslint-disable no-undef */
import React from 'react'

import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import { BrowserRouter as Router, Route } from 'react-router-dom-v5'

import { FileBrowser } from 'chonky'

import Directory from '../components/Directory'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Directory', () => {
  const wrapper = shallow(
    <Router>
      <Route
        exact
        path={'/category/:category'}
        render={(props) => <Directory {...props} />}
      />
    </Router>
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render a file browser component', () => {
    expect(wrapper.containsMatchingElement(<FileBrowser />))
  })

  it('should render link text to request a solution', () => {
    const link = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text() === 'Request Solution'
    })

    expect(link).toHaveLength(1)
  })

  it('should render link text to add a solution', () => {
    const link = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text() === 'Add Solution'
    })

    expect(link).toHaveLength(1)
  })
})
