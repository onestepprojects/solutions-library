/* eslint-disable no-undef */
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import { BrowserRouter as Router } from 'react-router-dom-v5'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Solutions', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Router />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
