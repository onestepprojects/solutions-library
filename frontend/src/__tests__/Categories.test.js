/* eslint-disable no-undef */
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'
import Search from '../common/Search'

import Categories from '../components/Categories'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Categories', () => {
  const routeComponentPropsMock = {
    history: {},
    location: {},
    match: {}
  }

  const wrapper = shallow(<Categories {...routeComponentPropsMock} />)

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render a search component', () => {
    expect(wrapper.containsMatchingElement(<Search />))
  })

  it('should render link text to add a new category', () => {
    const link = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text() === 'Add New Category'
    })

    expect(link).toHaveLength(1)
  })

  it('should render link text to see pending solutions', () => {
    const link = wrapper.findWhere((node) => {
      return (
        node.type() && node.name() && node.text() === 'Show Pending Solutions'
      )
    })

    expect(link).toHaveLength(1)
  })

  it('should render a card for each category', () => {
    const cards = wrapper.find('#categoryCards')

    expect(cards.children()).toHaveLength(2)
  })

  it('should render an extra card when Add New Category is clicked', () => {
    const link = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text() === 'Add New Category'
    })

    link.simulate('click')

    const cards = wrapper.find('#categoryCards')

    expect(cards.children()).toHaveLength(3)
  })
})
