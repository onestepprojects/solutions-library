/* eslint-disable no-undef */
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import EditSolution from '../components/EditSolution'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Categories', () => {
  const routeComponentPropsMock = {
    history: {},
    location: {},
    match: {
      params: {
        solution: 'solution-1',
        category: 'category-1'
      }
    }
  }

  const wrapper = shallow(<EditSolution {...routeComponentPropsMock} />)

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render text listing the solution name', () => {
    const text = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text().includes('Solution:')
    })

    expect(text).toHaveLength(1)
  })

  it('should render text listing the solution category', () => {
    const text = wrapper.findWhere((node) => {
      return node.type() && node.name() && node.text().includes('Category:')
    })

    expect(text).toHaveLength(1)
  })

  it('should render a form', () => {
    const tag = wrapper.find(form)

    expect(tag).toHaveLength(1)
  })
})
