import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom-v5'

import { ChonkyActions, setChonkyDefaults } from 'chonky'
import { ChonkyIconFA } from 'chonky-icon-fontawesome'

import './index.css'

import Categories from './components/Categories'
import Pending from './components/Pending'
import Directory from './components/Directory'
import SolutionForum from './components/SolutionForum'
import RequestSolution from './components/RequestSolution'
import NewSolution from './components/NewSolution'
import Solution from './components/Solution'
import EditSolution from './components/EditSolution'
import History from './components/History'

const Solutions = ({ match, authHelper, person }) => {
  setChonkyDefaults({
    iconComponent: ChonkyIconFA,
    defaultSortActionId: ChonkyActions.SortFilesByDate.id,
    disableDefaultFileActions: [
      ChonkyActions.SelectAllFiles.id,
      ChonkyActions.SelectAllFiles.id,
      ChonkyActions.EnableGridView.id,
      ChonkyActions.ToggleHiddenFiles.id,
      ChonkyActions.ClearSelection.id
    ],
    defaultFileViewActionId: ChonkyActions.EnableListView.id
  })

  useEffect(() => {
    if (!person) return

    const getPersonRoles = async () => {
      await authHelper.Get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations/get-org-by-person/${person.uuid}`
      )
    }

    getPersonRoles()
  }, [person])

  return (
    person && (
      <Router>
        <Switch>
          <Route
            exact
            path={`${match.url}/category/:category/request`}
            render={(props) => <RequestSolution {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/category/:category/:subcategory*/new`}
            render={(props) => <NewSolution {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/category/:category/:subcategory*/:solution/update`}
            render={(props) => <EditSolution {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/category/:category/:subcategory*/:solution/discussion/(general|implementations)`}
            render={(props) => (
              <SolutionForum {...props} auth={authHelper} person={person} />
            )}
          />
          <Route
            exact
            path={`${match.url}/category/:category/:subcategory*/:solution/history`}
            render={(props) => <History {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/category/:category/:subcategory*/:solution`}
            render={(props) => <Solution {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/category/:category`}
            render={(props) => <Directory {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={`${match.url}/pending`}
            render={(props) => <Pending {...props} auth={authHelper} />}
          />
          <Route
            exact
            path={match.url}
            render={(props) => <Categories {...props} auth={authHelper} />}
          />
        </Switch>
      </Router>
    )
  )
}

export default Solutions
