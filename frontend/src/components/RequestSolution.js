import React, { useState, useEffect } from 'react'

import { useParams } from 'react-router-dom-v5'

import TagsInput from 'react-tagsinput'
import { publishNode, getNode } from '../services/api/models/nodes'
import { createSolutionRequest } from '../services/api/models/solutions'

import './tagsinput.css'

// ROUTE: /solutions/category/:category/request
const RequestSolution = ({ history, auth }) => {
  // TODO: Implement Tags
  const { category } = useParams()
  const [categoryNode, setCategoryNode] = useState(null)

  const [tags, setTags] = useState([])
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')

  useEffect(() => {
    getNode(auth, category, '').then((data) => {
      if (data) {
        setCategoryNode(data)
      }
    })
  }, [])

  const onChange = (allTags) => {
    setTags(allTags)
  }

  const submitRequest = async (event) => {
    event.preventDefault()

    const request = await createSolutionRequest(
      auth,
      category,
      categoryNode.uuid,
      name,
      description
    )

    publishNode(auth, request.uuid, 'en').then(() => {
      history.goBack()
    })
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>Request Solution</h3>
        </div>
      </div>
      <div>
        <form>
          <div className='form-group'>
            <label htmlFor='requestName'>Solution Request Name</label>
            <input
              type='text'
              className='form-control'
              id='requestName'
              placeholder='Enter name'
              onChange={(event) => setName(event.target.value)}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='requestDescription'>
              Solution Request Description
            </label>
            <textarea
              className='form-control'
              id='requestDescription'
              placeholder='Enter description'
              rows='10'
              onChange={(event) => setDescription(event.target.value)}
            ></textarea>
            <TagsInput
              value={tags}
              onChange={onChange}
              onlyUnique
              inputProps={{
                placeholder: 'Add Tags'
              }}
            />
            <button
              type='submit'
              onClick={submitRequest}
              className='btn btn-primary mt-4'
            >
              Submit Request
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default RequestSolution
