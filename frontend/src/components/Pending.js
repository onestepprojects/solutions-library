import React, { useState, useEffect } from 'react'

import { ChonkyActions, FullFileBrowser } from 'chonky'

import {
  approve,
  viewSolutionDiscussionBoard
} from '../common/CustomFileActions'
import { getUnpublishedSolutions } from '../services/api/models/solutions'
import { publishNode } from '../services/api/models/nodes'

// ROUTE: /solutions/pending
const Pending = ({ match, history, location, auth }) => {
  const [files, setFiles] = useState([])

  useEffect(() => {
    getUnpublishedSolutions(auth).then((data) => {
      const fileData = data.map((value) => {
        return {
          id: value.uuid,
          name: value.fields.name,
          path: 'category' + value.path,
          request: value.fields.isRequest
        }
      })

      setFiles(fileData)
    })
  }, [])

  const handleFileActions = async (data) => {
    if (
      data.id === viewSolutionDiscussionBoard.id &&
      data.state.contextMenuTriggerFile
    ) {
      history.push(
        match.url +
          '/../' +
          data.state.contextMenuTriggerFile.path +
          '/discussion/general'
      )

      return
    } else if (data.id === viewSolutionDiscussionBoard.id) {
      history.push(
        match.url +
          '/../' +
          data.state.selectedFiles[0].path +
          '/discussion/general'
      )
      return
    }

    if (data.id === approve.id && data.state.contextMenuTriggerFile) {
      publishNode(auth, data.state.contextMenuTriggerFile.id, 'en').then(
        (response) => {
          if (response.published) {
            setFiles(
              files.filter((value) => {
                return value.id !== data.state.contextMenuTriggerFile.id
              })
            )
          }
        }
      )

      return
    } else if (data.id === approve.id) {
      publishNode(auth, data.state.selectedFiles[0].id, 'en').then(
        (response) => {
          if (response.published) {
            setFiles(
              files.filter((value) => {
                return value.id !== data.state.selectedFiles[0].id
              })
            )
          }
        }
      )

      return
    }

    let targetFile

    if (data.payload === undefined) {
      return
    }

    if (data.payload.targetFile !== undefined) {
      targetFile = data.payload.targetFile
    } else if (data.payload.files && data.payload.files[0] !== undefined) {
      targetFile = data.payload.files[0]
    } else {
      return
    }

    if (data.id === ChonkyActions.OpenFiles.id) {
      history.push(match.url + '/../' + targetFile.path)
      location.reload()
    }
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>Pending Solutions</h3>
        </div>
      </div>
      <div>
        <FullFileBrowser
          files={files}
          fileActions={[viewSolutionDiscussionBoard, approve]}
          onFileAction={handleFileActions}
        />
      </div>
    </div>
  )
}

export default Pending
