import React, { useState, createRef, useEffect } from 'react'
import { Link } from 'react-router-dom-v5'

import {
  getCategories,
  newCategory as createCategory
} from '../services/api/models/categories'

import { publishNode } from '../services/api/models/nodes'

import Search from '../common/Search'
import styles from './categories.module.css'
import { getAllSolutions } from '../services/api/models/solutions'
import { getCurrentUser } from '../services/api/models/auth'

// ROUTE: /solutions
const Categories = ({ match, history, auth }) => {
  const [isAdmin, setIsAdmin] = useState(false)

  const [categories, setCategories] = useState([])
  const [solutions, setSolutions] = useState([])

  const [curName, setCurName] = useState('')
  const [curDescription, setCurDescription] = useState('')
  const [newCategory, setNewCategory] = useState(false)

  const newCategoryRef = createRef()

  useEffect(() => {
    getCurrentUser(auth).then((data) => {
      if (data) {
        for (const item of data.groups) {
          if (item.name === 'admin') {
            setIsAdmin(true)
          }
        }
      }
    })
  }, [])

  useEffect(() => {
    getCategories(auth).then((data) => {
      if (data) {
        const newResult = data.node.children.elements.map((item) => {
          return {
            name: item.fields.name,
            description: item.fields.description,
            slug: item.fields.slug
          }
        })
        setCategories(newResult)
      }
    })
  }, [])

  useEffect(() => {
    if (newCategoryRef.current !== null) {
      newCategoryRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }, [newCategory])

  useEffect(() => {
    getAllSolutions(auth).then((data) => {
      if (data) {
        const newResult = data.nodes.elements.map((item) => {
          return {
            name: item.fields.name,
            slug: item.fields.slug,
            path: item.path
          }
        })
        setSolutions(newResult)
      }
    })
  }, [])

  const addCategory = () => {
    setNewCategory(true)
  }

  const submitCategory = () => {
    createCategory(auth, curName, curDescription).then((response) => {
      setCurName('')
      setCurDescription('')
      setNewCategory(false)

      publishNode(auth, response.uuid, response.language).then(() => {
        getCategories(auth).then((data) => {
          if (data) {
            const newResult = data.node.children.elements.map((item) => {
              return {
                name: item.fields.name,
                description: item.fields.description,
                slug: item.fields.slug
              }
            })
            setCategories(newResult)
          }
        })
      })
    })
  }

  const onDataSelected = ({ suggestionIndex, allSuggestions }) => {
    history.push(
      `${match.path}/category${allSuggestions[suggestionIndex].path}`
    )
  }

  const displayNewCategory = () => {
    return (
      <div ref={newCategoryRef} className={`pr-3 ${styles.card}`}>
        <div className={`card mt-3 w-100 ${styles.category}`}>
          <div className='card-body'>
            <div className='form-group'>
              <label htmlFor='categoryName'>Category Name</label>
              <input
                type='text'
                className='form-control'
                placeholder='Enter Category Name'
                id='categoryName'
                onChange={(e) => setCurName(e.target.value)}
              />
            </div>
            <div className='form-group'>
              <label htmlFor='categoryDescription'>Category Description</label>
              <input
                type='text'
                className='form-control'
                placeholder='Enter Category Description'
                id='categoryDescription'
                onChange={(e) => setCurDescription(e.target.value)}
              />
            </div>
            <button className='btn btn-primary' onClick={submitCategory}>
              Add Category
            </button>
          </div>
        </div>
      </div>
    )
  }

  const displayCategories = () => {
    if (categories && categories.length > 0) {
      return (
        <React.Fragment>
          <div
            className={`d-flex flex-wrap justify-content-center ${styles.categories}`}
            id='categoryCards'
          >
            {categories.map((category, index) => {
              return (
                <div className={`pr-3 ${styles.card}`} key={index}>
                  <Link
                    to={`${match.url}/category/${category.slug}`}
                    className='text-decoration-none'
                  >
                    <div className={`card mt-3 w-100 ${styles.category}`}>
                      <div className='card-body'>
                        <h4 className='card-title'>{category.name}</h4>
                        <h6 className='card-subtitle pt-2'>
                          {category.description}
                        </h6>
                      </div>
                    </div>
                  </Link>
                </div>
              )
            })}
            {newCategory && displayNewCategory()}
          </div>
        </React.Fragment>
      )
    } else {
      return (
        <div className='d-flex flex-column justify-content-center'>
          <h4 className={styles.noCategories}>No Available Categories</h4>
        </div>
      )
    }
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>Solutions Library</h3>
        </div>
        <div className='col-4'>
          <Search
            placeholder='Search Solutions'
            inputData={solutions}
            onDataSelected={onDataSelected}
          />
        </div>
      </div>
      {isAdmin && (
        <div className='d-flex flex-row justify-content-between'>
          <Link to={`${match.url}/pending`} className='text-decoration-none'>
            Show Pending Solutions
          </Link>

          <a onClick={addCategory} className='text-decoration-none link'>
            Add New Category
          </a>
        </div>
      )}
      {displayCategories()}
    </div>
  )
}

export default Categories
