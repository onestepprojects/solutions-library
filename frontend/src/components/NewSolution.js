import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom-v5'

import TagsInput from 'react-tagsinput'

import ContentSelector from '../common/ContentSelector'
import { getCurrentUser } from '../services/api/models/auth'
import { uploadDocument } from '../services/api/models/documents'
import { getNode } from '../services/api/models/nodes'
import { createSolution } from '../services/api/models/solutions'

import './tagsinput.css'

// ROUTE: /solutions/category/:category/:subcategory/new
const NewSolution = ({ auth }) => {
  const [user, setUser] = useState(null)
  const [categoryNode, setCategoryNode] = useState(null)

  const [tags, setTags] = useState([])

  const [name, setName] = useState('')
  const [cost, setCost] = useState(0)
  const [isoCode, setIsoCode] = useState('USD')
  const [hours, setHours] = useState('')
  const [description, setDescription] = useState('')
  const [requestId, setRequestId] = useState('')

  const [files, setFiles] = useState([])
  const [links, setLinks] = useState([])
  const [text, setText] = useState([])

  const { category } = useParams()
  const { subcategory } = useParams()

  const history = useHistory()

  useEffect(() => {
    getCurrentUser(auth).then((data) => {
      if (data) {
        setUser(data.uuid)
      }
    })
  }, [])

  useEffect(() => {
    getNode(auth, category, subcategory).then((data) => {
      if (data) {
        setCategoryNode(data)
      }
    })
  }, [])

  const onChange = (allTags) => {
    setTags(allTags)
  }

  const uploadFiles = async () => {
    const resultPromises = files.map(async (file) => {
      return await uploadDocument(auth, file.fileName, file.file)
    })

    // eslint-disable-next-line no-undef
    const data = await Promise.all(resultPromises)

    return data
  }

  const submitSolution = async (e) => {
    e.preventDefault()

    const fileData = await uploadFiles(auth)

    const solutionFields = {
      contributors: [user],
      description: description,
      documents: fileData,
      costValue: cost,
      costIsoCode: isoCode,
      inventors: [user],
      name: name,
      hours: hours,
      requestId: requestId,
      isRequest: false
    }

    let path = category
    if (subcategory) {
      path += '/' + subcategory
    }

    await createSolution(auth, path, categoryNode.uuid, solutionFields)
    history.push('/solutions')
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>New Solution</h3>
        </div>
      </div>
      <div>
        <form>
          <div className='form-group'>
            <label htmlFor='solutionName'>Solution Name</label>
            <input
              type='text'
              className='form-control'
              id='solutionName'
              onChange={(e) => setName(e.target.value)}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='requestId'>Request Id</label>
            <input
              type='text'
              className='form-control'
              id='requestId'
              onChange={(e) => setRequestId(e.target.value)}
            ></input>
          </div>
          <div className='form-row'>
            <div className='form-group col-1'>
              <input
                type='text'
                className='form-control'
                id='estimatedCost'
                value={cost}
                onChange={(event) => setCost(event.target.value)}
              ></input>
            </div>
            <div className='form-group col-1'>
              <select
                className='custom-select'
                id='isoCode'
                value={isoCode}
                onChange={(event) => setIsoCode(event.target.value)}
              >
                <option>USD</option>
                <option>EUR</option>
                <option>CHF</option>
              </select>
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='personHours'>Estimated Person Hours</label>
            <input
              type='text'
              className='form-control'
              id='personHours'
              onChange={(event) => setHours(event.target.value)}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='solutionDescription'>Solution Description</label>
            <textarea
              className='form-control'
              id='solutionDescription'
              rows='10'
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
            <ContentSelector
              files={files}
              setFiles={setFiles}
              links={links}
              setLinks={setLinks}
              text={text}
              setText={setText}
            />
            <TagsInput
              value={tags}
              onChange={onChange}
              onlyUnique
              inputProps={{
                placeholder: 'Add Tags'
              }}
            />
            <button
              type='submit'
              onClick={submitSolution}
              className='btn btn-primary mt-4'
            >
              Submit Solution
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default NewSolution
