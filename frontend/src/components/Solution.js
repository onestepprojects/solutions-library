/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react'
import { Link, useParams, useLocation } from 'react-router-dom-v5'

import { ChonkyActions, FileBrowser, FileList, FileContextMenu } from 'chonky'
import {
  downloadDocument,
  getDocumentDetails
} from '../services/api/models/documents'

import Ratings from 'react-ratings-declarative'

import { solutionWithVersion } from '../services/api/models/solutions'
import { getUserById } from '../services/api/models/auth'
import { getRating, submitRating } from '../services/api/models/ratings'

// ROUTE: /solutions/category/:category/:subcategory/:solution?version=""
const Solution = ({ match, auth }) => {
  // TODO: update/read Rating at server
  // TODO: input data/styling

  const query = new URLSearchParams(useLocation().search)

  const [rating, setRating] = useState(null)
  const [solutionDetails, setSolutionDetails] = useState(null)

  const [author, setAuthor] = useState('')
  const [contributors, setContributors] = useState([])

  const [files, setFiles] = useState([])

  const { solution } = useParams()
  const { category } = useParams()
  const { subcategory } = useParams()
  const version = query.get('version')

  useEffect(() => {
    solutionWithVersion(auth, category, subcategory, solution, version).then(
      (data) => {
        if (data) {
          setSolutionDetails(data)
        }
      }
    )
  }, [])

  useEffect(() => {
    if (!solutionDetails) {
      return
    }

    if (solutionDetails.fields.documents) {
      const getDetails = async () => {
        const newFiles = []
        for (const document of solutionDetails.fields.documents) {
          const data = await getDocumentDetails(auth, document.uuid)
          if (data) {
            newFiles.push({
              name: data.node.fields.binary.fileName,
              id: document.uuid,
              isDir: false,
              mimeType: data.node.fields.binary.mimeType
            })
          }
        }

        setFiles(newFiles)
      }

      getDetails()
    }

    if (solutionDetails.fields.links) {
      const links = solutionDetails.fields.links.map((data) => {
        return {
          placeholder: data.fields.placeholder,
          link: data.fields.url
        }
      })

      setLinks(links)
    }
  }, [solutionDetails])

  useEffect(() => {
    if (!solutionDetails) return

    getRating(auth, solutionDetails.uuid).then((data) => {
      if (data) {
        setRating(data)
      }
    })
  }, [solutionDetails])

  useEffect(() => {
    if (!solutionDetails) return

    getUserById(auth, solutionDetails.creator.uuid).then((data) => {
      if (data) {
        setAuthor(data.firstname ? data.firstname : data.username)
      }
    })
  }, [solutionDetails])

  useEffect(() => {
    if (!solutionDetails) return

    const getContributors = async () => {
      if (solutionDetails.fields.contributors) {
        const result = await Promise.all(
          solutionDetails.fields.contributors.map(async (item) => {
            const user = await getUserById(auth, item)

            return user.firstname ? user.firstname : user.username
          })
        )

        setContributors(result)
      }
    }

    getContributors()
  }, [solutionDetails])

  const setUserRating = async (newRating) => {
    await submitRating(auth, rating.uuid, newRating)

    getRating(auth, solutionDetails.uuid).then((data) => {
      if (data) {
        setRating(data)
      }
    })
  }

  /*   const deleteSolution = () => {
    // TODO: delete solution on server
    // TODO: ask to confirm
  } */

  const handleFileActions = (data) => {
    if (data.payload === undefined && data.state === undefined) {
      return
    }

    if (data.id === ChonkyActions.OpenFiles.id) {
      downloadDocument(auth, data.payload.files[0].id, 'binary').then(
        (blob) => {
          const url = window.URL.createObjectURL(
            new Blob([blob], {
              type: data.payload.files[0].mimeType
            })
          )
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('target', '_blank')
          document.body.appendChild(link)
          link.click()
          link.parentNode.removeChild(link)
          window.URL.revokeObjectURL(url)
        }
      )
    } else if (data.id === ChonkyActions.OpenSelection.id) {
      downloadDocument(
        auth,
        data.state.contextMenuTriggerFile.id,
        'binary'
      ).then((blob) => {
        const url = window.URL.createObjectURL(
          new Blob([blob], {
            type: data.state.contextMenuTriggerFile.mimeType
          })
        )
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('target', '_blank')
        document.body.appendChild(link)
        link.click()
        link.parentNode.removeChild(link)
        window.URL.revokeObjectURL(url)
      })
    }
  }

  return (
    <React.Fragment>
      {solutionDetails && author && (
        <div className='main-content-container container-fluid px-4'>
          <div
            className='page-header row no-gutters pt-4 pb-1 mb-3 border-bottom justify-content-between'
            style={{ position: 'relative' }}
          >
            <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
              <span className='text-uppercase page-subtitle'>
                {solutionDetails.fields.isRequest
                  ? 'Solution Request'
                  : 'Solutions'}
              </span>
              <h3 className='page-title'>{solutionDetails.fields.name}</h3>
              <div className='d-flex flex-column mt-4'>
                <Link
                  className='text-decoration-none mb-2'
                  to={`${match.url}/update`}
                >
                  Edit Solution
                </Link>
              </div>
            </div>
            <div className='col-md-auto text-sm-left mb-0'>
              <div>
                {rating && (
                  <div>
                    <Ratings
                      rating={rating.data.userRating}
                      changeRating={setUserRating}
                      widgetDimensions='17px'
                      widgetRatedColors='#DAA520'
                      widgetHoverColors='#DAA520'
                    >
                      <Ratings.Widget />
                      <Ratings.Widget />
                      <Ratings.Widget />
                      <Ratings.Widget />
                      <Ratings.Widget />
                    </Ratings>
                    <p className='mt-2'>
                      Average Rating: {rating.data.avgRating}
                    </p>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className='card mt-2'>
            <div className='card-body'>
              <div className='row'>
                <div className='col'>
                  <h6>Name: {solutionDetails.fields.name}</h6>
                  <h6>ID: {solutionDetails.uuid}</h6>
                  <h6>Description: {solutionDetails.fields.description}</h6>
                </div>
                <div className='col'>
                  <h6>Author: {author}</h6>
                  <h6>Created: {solutionDetails.created}</h6>
                  <h6>Last Updated: {solutionDetails.edited}</h6>
                  <h6>Version: {solutionDetails.version}</h6>
                  <Link
                    className='text-decoration-none link'
                    to={`${match.url}/history`}
                  >
                    Solution History
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className='card my-3'>
            <FileBrowser
              files={files}
              defaultFileViewActionId={ChonkyActions.EnableGridView.id}
              onFileAction={handleFileActions}
            >
              <FileList />
              <FileContextMenu />
            </FileBrowser>
          </div>
          <div className='row'>
            <div className='col-10'></div>
            <div className='col-2'>
              <div>
                <Link
                  className='text-decoration-none link'
                  to={`${match.url}/discussion/general`}
                >
                  General Discussion Board
                </Link>
              </div>
              <div>
                <Link
                  className='text-decoration-none link'
                  to={`${match.url}/discussion/implementations`}
                >
                  Implementations Discussion Board
                </Link>
              </div>
              <div>
                {contributors.length > 0 &&
                  `Edited By: ${contributors.join(', ')}`}
              </div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  )
}

export default Solution
