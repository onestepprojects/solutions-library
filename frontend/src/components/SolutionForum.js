import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom-v5'

import DiscussionBoard from 'react-discussion-board'

import 'react-discussion-board/dist/index.css'
import {
  addPostToDiscussion,
  getComments,
  newPost
} from '../services/api/models/discussion'
import { getSolution } from '../services/api/models/solutions'
import { getCurrentUser } from '../services/api/models/auth'
import { publishNode } from '../services/api/models/nodes'

// ROUTE: /solutions/category/:category/:subcategory/:solution/discussion/general
// ROUTE: /solutions/category/:category/:subcategory/:solution/discussion/implementations
const SolutionForum = ({ match, auth, person }) => {
  // TODO: Work on styling
  // TODO: automated messages
  // TODO: Shortcuts to messages

  const [forumType, setForumType] = useState('general')
  const [posts, setPosts] = useState([])
  const [solutionDetails, setSolutionDetails] = useState(null)
  const [curUser, setCurUser] = useState(null)

  const { solution } = useParams()
  const { category } = useParams()
  const { subcategory } = useParams()

  useEffect(() => {
    const parsedMatch = match.url.split('/')

    setForumType(parsedMatch[parsedMatch.length - 1])
  }, [])

  useEffect(() => {
    getSolution(auth, category, subcategory, solution).then((data) => {
      if (data) {
        setSolutionDetails(data)
      }
    })
  }, [])

  useEffect(() => {
    getCurrentUser(auth).then((data) => {
      if (data) {
        setCurUser({
          id: data.uuid,
          firstName: data.firstname,
          username: data.username
        })
      }
    })
  }, [])

  useEffect(() => {
    const getProfileImage = async () => {
      if (!curUser) return

      const data = await auth.Get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/persons/${person.uuid}`
      )

      if (data.data) {
        setCurUser({
          ...curUser,
          image: data.data.profilePicture.name
            ? data.data.profilePicture.url
            : null
        })
      }
    }

    getProfileImage()
  }, [curUser])

  useEffect(() => {
    if (!solutionDetails || !curUser) {
      return
    }

    getComments(auth, solutionDetails.uuid, forumType).then((data) => {
      if (data.node.fields.comments.fields.comments) {
        const comments = data.node.fields.comments.fields.comments.map(
          (comment) => {
            return {
              uuid: comment.uuid,
              profileImage: curUser.image,
              name: comment.creator.firstname
                ? comment.creator.firstname
                : comment.creator.username,
              content: comment.fields.value,
              date: new Date(comment.created)
            }
          }
        )

        setPosts(comments)
      }
    })
  }, [solutionDetails, curUser])

  const submitPost = async (text) => {
    const commentUuid =
      forumType === 'general'
        ? solutionDetails.fields.comments.uuid
        : solutionDetails.fields.feedback.uuid
    const data = await newPost(auth, text)

    await publishNode(auth, data.uuid, data.language)

    const board = await addPostToDiscussion(
      auth,
      data.uuid,
      commentUuid,
      posts.map((post) => {
        return { uuid: post.uuid }
      })
    )

    await publishNode(auth, board.uuid, board.language)

    const curDate = new Date()

    setPosts([
      ...posts,
      {
        uuid: commentUuid,
        profileImage: curUser.image,
        name: curUser.firstName ? curUser.firstName : curUser.username,
        content: text,
        date: curDate
      }
    ])
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>Discussion Board</h3>
        </div>
      </div>
      <DiscussionBoard posts={posts} onSubmit={submitPost} />
    </div>
  )
}

export default SolutionForum
