import React, { useEffect, useState } from 'react'

import { Link, useParams } from 'react-router-dom-v5'
import { Modal, Button } from 'react-bootstrap'

import { ChonkyActions, FullFileBrowser } from 'chonky'

import {
  viewSolutionDiscussionBoard,
  addNewSubcategory
} from '../common/CustomFileActions'
import { getChildren, publishNode } from '../services/api/models/nodes'
import { getCategory } from '../services/api/models/categories'
import { postNewSubcategory } from '../services/api/models/subcategory'

import { ChonkyRequestIcon } from '../common/ChonkyRequestIcon'

// ROUTE: /solutions/category/:category
const Directory = ({ history, match, auth }) => {
  const [files, setFiles] = useState([])
  const [folderChain, setFolderChain] = useState([])

  const { category } = useParams()

  const [currentPath, setCurrentPath] = useState([category])
  const [categoryData, setCategoryData] = useState(null)
  const [currentLeaf, setCurrentLeaf] = useState(null)

  const [isFolderChain, setIsFolderChain] = useState(false)
  const [displayNewSubcategory, setDisplayNewSubcategory] = useState(false)
  const [newSubCategory, setNewSubcategory] = useState('')

  useEffect(() => {
    getCategory(auth, category).then((data) => {
      setCategoryData(data)
      setFolderChain([
        {
          name: data.fields.name,
          id: data.uuid,
          isDir: true
        }
      ])
    })
  }, [])

  useEffect(() => {
    updateFiles()
    if (currentLeaf && !isFolderChain) {
      setFolderChain([...folderChain, currentLeaf])
    }

    setIsFolderChain(false)
  }, [currentPath])

  const updateFiles = () => {
    getChildren(auth, '/' + currentPath.join('/')).then((data) => {
      if (data) {
        const newResult = data.node.children.elements
          .filter((value) => {
            return value.isPublished
          })
          .map((item) => {
            return {
              name: item.fields.name,
              slug: item.fields.slug,
              id: item.uuid,
              isDir: item.isContainer,
              modDate: !item.isContainer ? item.edited : null,
              ...(item.fields.isRequest && { icon: 'requestIcon' })
            }
          })

        setFiles(newResult)
      }
    })
  }

  const handleClose = (closeType) => {
    if (closeType === 'submit') {
      postNewSubcategory(
        auth,
        '/' + currentPath.join('/'),
        newSubCategory
      ).then((data) => {
        if (data) {
          const newResult = {
            name: newSubCategory,
            slug: data.slug,
            id: data.uuid,
            isDir: true
          }
          setFiles([...files, newResult])
          publishNode(auth, data.uuid, data.language)
        }
      })
    }

    setDisplayNewSubcategory(false)
  }

  const addSubcategory = () => {
    return (
      <Modal show={displayNewSubcategory} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>New Subcategory</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <input
              type='text'
              id='subcategory'
              placeholder='Subcategory Name'
              className='form-control'
              value={newSubCategory}
              onChange={(event) => {
                setNewSubcategory(event.target.value)
              }}
            />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => handleClose('close')}>
            Cancel
          </Button>
          <Button variant='primary' onClick={() => handleClose('submit')}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }

  const returnToAncestor = (data) => {
    setCurrentLeaf(data.payload.targetFile)
    const newCurrentPath = currentPath.slice()
    const newFolderChain = folderChain.slice()

    newCurrentPath.pop()

    while (
      currentLeaf.id !== newFolderChain.pop().id &&
      folderChain.length > 0
    ) {
      setCurrentLeaf(newFolderChain[newFolderChain.length - 1])
      newFolderChain.pop()

      newCurrentPath.pop()
    }

    setIsFolderChain(true)
    setCurrentPath(newCurrentPath)
    setFolderChain(newFolderChain)
  }

  const handleFileActions = (data) => {
    if (
      data.id === viewSolutionDiscussionBoard.id &&
      data.state.contextMenuTriggerFile
    ) {
      history.push(
        match.url +
          (currentPath.length > 1 ? '/' : '') +
          currentPath.slice(1).join('/') +
          '/' +
          data.state.contextMenuTriggerFile.slug +
          '/discussion/general'
      )
      return
    } else if (data.id === viewSolutionDiscussionBoard.id) {
      history.push(
        match.url +
          (currentPath.length > 1 ? '/' : '') +
          currentPath.slice(1).join('/') +
          '/' +
          data.state.selectedFiles[0].slug +
          '/discussion/general'
      )
    } else if (data.id === addNewSubcategory.id) {
      setDisplayNewSubcategory(true)
      return
    }

    let targetFile

    if (data.payload === undefined) {
      return
    }

    if (data.payload.targetFile !== undefined) {
      targetFile = data.payload.targetFile
    } else if (data.payload.files && data.payload.files[0] !== undefined) {
      targetFile = data.payload.files[0]
    } else {
      return
    }

    if (data.id === ChonkyActions.OpenFiles.id && targetFile.isDir) {
      if (files.find((item) => item.id === targetFile.id) !== undefined) {
        setCurrentLeaf(targetFile)
        setCurrentPath([...currentPath, targetFile.slug])
      } else {
        returnToAncestor(data)
      }
    } else if (data.id === ChonkyActions.OpenFiles.id) {
      history.push(
        match.url +
          (currentPath.length > 1 ? '/' : '') +
          currentPath.slice(1).join('/') +
          '/' +
          data.payload.files[0].slug
      )
    }
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>
            {categoryData && categoryData.fields.name}
          </h3>
        </div>
      </div>
      <div className='d-flex flex-column'>
        <Link to={`${match.url}/request`} className='text-decoration-none'>
          Request Solution
        </Link>
        <Link className='text-decoration-none mb-2' to={`${match.url}/new`}>
          New Solution
        </Link>
      </div>
      <div>
        <FullFileBrowser
          files={files}
          folderChain={folderChain}
          fileActions={[viewSolutionDiscussionBoard, addNewSubcategory]}
          onFileAction={handleFileActions}
          iconComponent={ChonkyRequestIcon}
        />
      </div>
      {addSubcategory()}
    </div>
  )
}

export default Directory
