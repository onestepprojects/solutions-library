import React, { useEffect, useState } from 'react'

import TagsInput from 'react-tagsinput'

import { useHistory, useParams } from 'react-router-dom-v5'
import ContentSelector from '../common/ContentSelector'
import {
  buildSolution,
  getSolution,
  getSolutionByVersion,
  updateSolution
} from '../services/api/models/solutions'
import {
  getDocumentDetails,
  uploadDocument
} from '../services/api/models/documents'
import { getCurrentUser } from '../services/api/models/auth'

import './tagsinput.css'
import { publishNode } from '../services/api/models/nodes'

// ROUTE: /solutions/category/:category/:subcategory/:solution/update
const EditSolution = ({ auth }) => {
  // TODO: support resources/skills
  // TODO: loading icon
  // TODO: Error handlng
  // TODO: filename duplicates
  const history = useHistory()

  const [solutionDetails, setSolutionDetails] = useState(null)
  const [user, setUser] = useState('')

  const [updateMessage, setUpdateMessage] = useState('')
  const [cost, setCost] = useState(0)
  const [isoCode, setIsoCode] = useState('USD')
  const [hours, setHours] = useState('')
  const [description, setDescription] = useState('')

  const [tags, setTags] = useState([])

  const [files, setFiles] = useState([])
  const [links, setLinks] = useState([])
  const [text, setText] = useState([])

  const { solution } = useParams()
  const { category } = useParams()
  const { subcategory } = useParams()

  useEffect(() => {
    getCurrentUser(auth).then((data) => {
      if (data) {
        setUser(data.uuid)
      }
    })
  }, [])

  useEffect(() => {
    getSolution(auth, '/' + category, subcategory, solution).then((data) => {
      if (data) {
        getSolutionByVersion(auth, data.uuid, 'published').then((published) => {
          setSolutionDetails(published)
        })
      }
    })
  }, [])

  useEffect(() => {
    if (!solutionDetails) {
      return null
    }

    if (solutionDetails.fields.estimatedCost) {
      setCost(solutionDetails.fields.estimatedCost.fields.costValue)
      setIsoCode(solutionDetails.fields.estimatedCost.fields.isoCode)
    }

    if (solutionDetails.fields.hours) {
      setHours(solutionDetails.fields.hours)
    }

    if (solutionDetails.fields.description) {
      setDescription(solutionDetails.fields.description)
    }

    if (solutionDetails.fields.documents) {
      setFiles([])
      for (const document of solutionDetails.fields.documents) {
        getDocumentDetails(auth, document.uuid).then((data) => {
          if (data) {
            setFiles([
              ...files,
              {
                uuid: document.uuid,
                fileName: data.node.fields.binary.fileName
              }
            ])
          }
        })
      }
    }

    if (solutionDetails.fields.links) {
      const links = solutionDetails.fields.links.map((data) => {
        return {
          placeholder: data.fields.placeholder
            ? data.fields.placeholder
            : data.fields.url,
          link: data.fields.url
        }
      })

      setLinks(links)
    }
  }, [solutionDetails])

  const uploadFiles = async () => {
    const resultPromises = files.map(async (file) => {
      if (file.uuid) {
        return { uuid: file.uuid }
      }

      return await uploadDocument(auth, file.fileName, file.file)
    })

    // eslint-disable-next-line no-undef
    const data = await Promise.all(resultPromises)

    return data
  }

  const updateData = async (e) => {
    e.preventDefault()

    const fileIds = await uploadFiles(auth)

    const newSolution = buildSolution({
      comments: solutionDetails.fields.comments,
      contributors: solutionDetails.fields.contributors,
      additionalContributor: user,
      description: description,
      documents: fileIds,
      text: text,
      links: links,
      costValue: cost,
      costIsoCode: isoCode,
      feedback: solutionDetails.fields.feedback,
      inventors: solutionDetails.fields.inventors,
      name: solutionDetails.fields.name,
      hours: hours,
      isRequest: solutionDetails.fields.isRequest,
      requestId: solutionDetails.fields.requestId,
      updateMessage: updateMessage
    })

    updateSolution(auth, solutionDetails.uuid, newSolution).then(async () => {
      if (solutionDetails.fields.isRequest) {
        await publishNode(auth, solutionDetails.uuid, solutionDetails.language)
      }

      let sub

      if (subcategory === undefined) {
        sub = ''
      } else {
        sub = '/' + subcategory
      }

      history.push(`${category}${sub}/${solution}`)
    })
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>Edit Solution</h3>
        </div>
        <div className='col-md-auto text-sm-left mb-0'>
          <div>Solution: {solutionDetails && solutionDetails.displayName}</div>
          <div>Category: {category}</div>
        </div>
      </div>
      <div>
        {solutionDetails && (
          <form>
            <div className='form-group'>
              <label htmlFor='updateMessage'>Solution Update Message</label>
              <input
                type='text'
                className='form-control'
                id='updateMessage'
                value={updateMessage}
                onChange={(event) => setUpdateMessage(event.target.value)}
              ></input>
            </div>
            <div className='form-row pl-1'>
              <label htmlFor='estimatedCost'>Estimated Cost</label>
            </div>
            <div className='form-row'>
              <div className='form-group col-1'>
                <input
                  type='text'
                  className='form-control'
                  id='estimatedCost'
                  value={cost}
                  onChange={(event) => setCost(event.target.value)}
                ></input>
              </div>
              <div className='form-group col-1'>
                <select
                  className='custom-select'
                  id='isoCode'
                  value={isoCode}
                  onChange={(event) => setIsoCode(event.target.value)}
                >
                  <option>USD</option>
                  <option>EUR</option>
                  <option>CHF</option>
                </select>
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-2'>
                <label htmlFor='personHours'>Estimated Person Hours</label>
                <input
                  type='text'
                  className='form-control'
                  id='personHours'
                  value={hours}
                  onChange={(event) => setHours(event.target.value)}
                ></input>
              </div>
            </div>
            <div className='form-group'>
              <label htmlFor='solutionDescription'>Solution Description</label>
              <textarea
                className='form-control'
                id='solutionDescription'
                rows='10'
                value={description}
                onChange={(event) => setDescription(event.target.value)}
              ></textarea>
              <ContentSelector
                files={files}
                setFiles={setFiles}
                links={links}
                setLinks={setLinks}
                text={text}
                setText={setText}
              />
              <TagsInput
                value={tags}
                onChange={(allTags) => setTags(allTags)}
                onlyUnique
                inputProps={{
                  placeholder: 'Add Tags'
                }}
              />
              <button onClick={updateData} className='btn btn-primary mt-4'>
                Update Solution
              </button>
            </div>
          </form>
        )}
      </div>
    </div>
  )
}

export default EditSolution
