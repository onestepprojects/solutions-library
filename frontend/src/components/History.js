import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom-v5'

import {
  VerticalTimeline,
  VerticalTimelineElement
} from 'react-vertical-timeline-component'
import 'react-vertical-timeline-component/style.min.css'
import {
  getSolutionByVersion,
  getSolutionVersions
} from '../services/api/models/solutions'

import style from './history.module.css'

// ROUTE: /solutions/category/:category/:subcategory/:solution/history
const History = ({ history, auth }) => {
  // TODO: get versions for node
  // Must be sorted

  const [versions, setVersions] = useState([])

  const { solution } = useParams()
  const { category } = useParams()
  const { subcategory } = useParams()

  useEffect(async () => {
    const result = await getSolutionVersions(
      auth,
      category,
      subcategory,
      solution
    )

    const filteredResult = result.node.versions.filter((version) => {
      return version.version[version.version.length - 1] === '0'
    })

    // eslint-disable-next-line no-undef
    const newResult = await Promise.all(
      filteredResult.map(async (item) => {
        const solution = await getSolutionByVersion(
          auth,
          result.node.uuid,
          item.version
        )

        return {
          name: item.version,
          created: item.created,
          creator: {
            firstName: item.creator.firstname,
            username: item.creator.username
          },
          message: solution.fields.updateMessage
        }
      })
    )
    setVersions(newResult)
  }, [])

  const onClick = (version) => {
    let subcategoryPath = ''

    if (subcategory) {
      subcategoryPath = '/' + subcategory
    }

    history.push(
      `/solutions/category/${category}${subcategoryPath}/${solution}?version=${version}`
    )
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Solutions</span>
          <h3 className='page-title'>History</h3>
        </div>
      </div>
      <VerticalTimeline layout='1-column-left'>
        {versions.map((version, index) => {
          return (
            <VerticalTimelineElement
              className={style.timeline}
              key={index}
              date={version.created}
              onTimelineElementClick={() => onClick(version.name)}
              iconStyle={{ background: '#007bff' }}
            >
              <h3>Version {version.name}</h3>
              <h3>
                {version.creator.firstName
                  ? version.creator.firstName
                  : version.creator.username}
              </h3>
              <h4>{version.message}</h4>
            </VerticalTimelineElement>
          )
        })}
      </VerticalTimeline>
    </div>
  )
}

export default History
