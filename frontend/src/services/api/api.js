/* eslint-disable no-undef */
const apiUrl = process.env.REACT_APP_SOLUTIONS_API_URL

export function graphQl(auth, query, variables) {
  return post(auth, `/solutions/graphql`, {
    query,
    variables
  }).then((response) => response.data)
}

export async function get(auth, path, headers = {}) {
  let token = await auth.GetUserIdToken()
  return fetch(`${apiUrl}${path}`, {
    headers: { ...headers, Authorization: `Bearer ${token}` }
  }).then((response) => response.json())
}

export async function get_file(auth, path) {
  let token = await auth.GetUserIdToken()
  return fetch(`${apiUrl}${path}`, {
    headers: { 'Content-Type': 'blob', Authorization: `Bearer ${token}` }
  }).then((response) => response.blob())
}

export async function post(auth, path, data) {
  let token = await auth.GetUserIdToken()

  return fetch(`${apiUrl}${path}`, {
    body: JSON.stringify(data),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  }).then((response) => response.json())
}

export async function upload_file(
  auth,
  path,
  file,
  name,
  filename,
  version,
  language
) {
  let token = await auth.GetUserIdToken()
  const formData = new FormData()
  formData.append('version', version)
  formData.append('language', language)
  formData.append(name, file, filename)

  return fetch(`${apiUrl}${path}`, {
    body: formData,
    method: 'POST',
    headers: { Authorization: `Bearer ${token}` }
  }).then((response) => response.json())
}
