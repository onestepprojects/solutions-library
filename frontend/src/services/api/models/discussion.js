import { graphQl, post } from '../api'

export async function getComments(auth, uuid, boardType) {
  var fieldName = 'comments'

  if (boardType !== 'general') {
    fieldName = 'feedback'
  }

  return graphQl(
    auth,
    `{
    node(uuid: "${uuid}") {
      ... on solution {
        fields {
          ${fieldName} {
            ... on discussion {
              uuid
              fields {
                comments {
                  ... on comment {
                    uuid
                    created
                    creator {
                      uuid
                      firstname
                      username
                    }
                    fields {
                      value
                    }
                  }
                }
              }
            }
          }
        }
      } 
    }
  }`
  )
}

export async function newPost(auth, text) {
  var id = Math.random().toString(36).substring(2, 15)

  return post(auth, `/solutions/webroot/comments/${id}`, {
    language: 'en',
    schema: {
      name: 'comment'
    },
    fields: {
      id: id,
      value: text
    }
  }).then((data) => {
    return { uuid: data.uuid, language: data.language }
  })
}

export async function addPostToDiscussion(auth, uuid, boardUuid, comments) {
  if (!comments) {
    comments = [{ uuid: uuid }]
  } else {
    comments = [...comments, { uuid: uuid }]
  }

  return post(auth, `/solutions/nodes/${boardUuid}`, {
    language: 'en',
    fields: {
      comments: comments
    }
  })
}

export async function createDiscussion(auth, path, name) {
  const randString = Math.random().toString(36).substring(2, 15)

  return post(auth, `/solutions/webroot/${path}/${randString}`, {
    language: 'en',
    schema: {
      name: 'discussion'
    },
    fields: {
      id: randString,
      name: name
    }
  }).then((data) => {
    if (!data) {
      throw new Error('failure')
    } else {
      return { uuid: data.uuid, language: data.language }
    }
  })
}
