import { post } from '../api'
import { slugify } from '../util'

export async function postNewSubcategory(auth, path, name) {
  const slug = slugify(name)
  return post(auth, `/solutions/webroot/${path}/${slug}`, {
    language: 'en',
    schema: {
      name: 'sub_category'
    },
    fields: {
      name: name,
      slug: slug
    }
  }).then((data) => {
    return { uuid: data.uuid, slug: data.slug, language: data.language }
  })
}
