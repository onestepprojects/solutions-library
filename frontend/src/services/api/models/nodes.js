import { graphQl, post, get } from '../api'

export async function publishNode(auth, uuid, language) {
  return post(
    auth,
    `/solutions/nodes/${uuid}/languages/${language}/published`,
    {}
  )
}

export async function getChildren(auth, path) {
  return graphQl(
    auth,
    `{
    node(path: "${path}") {
      children(filter: {
        or: [
          {
            schema: {
              is: solution
            } 
          },
          {
            schema: {
              is: sub_category
            } 
          }
        ]
  
      }) {
        elements {
          uuid
          isContainer
          isPublished
          edited
          ... on solution {
            fields {
              name
              slug
              isRequest
            }
          }
          ... on sub_category {
            fields {
              name
              slug
            }
          }
        }
      }
    }
  }`
  )
}

export async function getNode(auth, category, subcategory) {
  if (subcategory === undefined) {
    subcategory = ''
  } else {
    subcategory = '/' + subcategory
  }

  return get(auth, `/solutions/webroot/${category}${subcategory}`)
}
