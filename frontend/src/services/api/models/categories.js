import { graphQl, post, get } from '../api'
import { slugify } from '../util'

export async function getCategories(auth) {
  return graphQl(
    auth,
    `{
    node(path: "/") {
      children(filter: {
          schema: {
              is: category
          }
      }) {
          elements {
              uuid
              ... on category {
                  fields {
                      name
                      description
                      slug
                  }
              }
          }
      }
    }
  }`
  )
}

export async function getCategory(auth, category) {
  return get(auth, `/solutions/webroot/${category}`)
}

export async function newCategory(auth, name, description) {
  const slug = slugify(name)
  return post(auth, `/solutions/webroot/${slug}`, {
    language: 'en',
    schema: {
      name: 'category'
    },
    fields: {
      description: description,
      name: name,
      slug: slug
    }
  }).then((data) => {
    if (!data.uuid) {
      throw new Error('failure')
    } else {
      return { uuid: data.uuid, language: data.language }
    }
  })
}
