import { graphQl, get, post } from '../api'
import { slugify } from '../util'
import { createDiscussion } from './discussion'
import { publishNode } from './nodes'
import { createRating } from './ratings'

export async function getAllSolutions(auth) {
  return graphQl(
    auth,
    `{
    nodes(filter: {
      schema: { is: solution }
    }) {
      elements {
        uuid
        path
        isPublished
        ... on solution {
          fields {
            name
            slug
            isRequest
          }
        }
      }
    }
  }`
  )
}

export async function getUnpublishedSolutions(auth) {
  return getAllSolutions(auth).then((data) => {
    return data.nodes.elements.filter((value) => {
      return value.isPublished === false
    })
  })
}

export async function getSolution(auth, category, subcategory, name) {
  if (subcategory === undefined) {
    subcategory = ''
  } else {
    subcategory = '/' + subcategory
  }

  return get(auth, `/solutions/webroot/${category}${subcategory}/${name}`)
}

export async function getSolutionByVersion(auth, uuid, version) {
  return get(auth, `/solutions/nodes/${uuid}?version=${version}`)
}

export async function solutionWithVersion(
  auth,
  category,
  subcategory,
  name,
  version
) {
  const solution = await getSolution(auth, category, subcategory, name)

  if (version) {
    return getSolutionByVersion(auth, solution.uuid, version)
  }

  return solution
}

export async function getSolutionVersions(auth, category, subcategory, name) {
  if (subcategory === undefined) {
    subcategory = ''
  } else {
    subcategory = '/' + subcategory
  }

  const path = '/' + category + subcategory + '/' + name

  return graphQl(
    auth,
    `{ 
    node(path: "${path}") {
      uuid
      versions {
        version
        draft
        published
        branchRoot
        created
        creator {
          uuid
          username
          firstname
        }
      }
    }
  }
`
  )
}

export async function createSolution(auth, path, pathUuid, fields) {
  const solution = buildSolution(fields)

  const general = await createDiscussion(
    auth,
    path,
    solution.fields.slug + '-general'
  )
  if (general.uuid) {
    await publishNode(auth, general.uuid, general.language)
  } else {
    return
  }

  const feedback = await createDiscussion(
    auth,
    path,
    solution.fields.slug + '-feedback'
  )
  if (feedback.uuid) {
    await publishNode(auth, feedback.uuid, feedback.language)
  } else {
    return
  }

  const rating = await createRating(auth, pathUuid)
  if (rating.uuid) {
    await publishNode(auth, rating.uuid, rating.language)
  } else {
    return
  }

  solution.fields.comments = { uuid: general.uuid }
  solution.fields.feedback = { uuid: feedback.uuid }
  solution.fields.ratings = { uuid: rating.uuid }

  return post(
    auth,
    `/solutions/webroot/${path}/${solution.fields.slug}`,
    solution
  ).then((data) => {
    return { uuid: data.uuid, lanaguage: data.langauge }
  })
}

export async function createSolutionRequest(
  auth,
  path,
  pathUuid,
  requestName,
  requestMessage
) {
  const solution = buildSolution({
    name: requestName,
    description: requestMessage,
    isRequest: true
  })

  const general = await createDiscussion(
    auth,
    path,
    solution.fields.slug + '-general'
  )
  if (general.uuid) {
    await publishNode(auth, general.uuid, general.language)
  } else {
    return
  }

  const feedback = await createDiscussion(
    auth,
    path,
    solution.fields.slug + '-feedback'
  )
  if (feedback.uuid) {
    await publishNode(auth, feedback.uuid, feedback.language)
  } else {
    return
  }

  const rating = await createRating(auth, pathUuid)
  if (rating.uuid) {
    await publishNode(auth, rating.uuid, rating.language)
  } else {
    return
  }

  solution.fields.comments = { uuid: general.uuid }
  solution.fields.feedback = { uuid: feedback.uuid }
  solution.fields.ratings = { uuid: rating.uuid }

  return post(
    auth,
    `/solutions/webroot/${path}/${solution.fields.slug}`,
    solution
  ).then((data) => {
    return { uuid: data.uuid, language: data.language }
  })
}

export async function updateSolution(auth, uuid, solution) {
  return post(auth, `/solutions/nodes/${uuid}`, solution).then((data) => {
    return { uuid: data.uuid, language: data.language }
  })
}

export async function updateSolutionField(auth, uuid, name, value) {
  const solution = await get(auth, `/solutions/nodes/${uuid}`)

  if (solution) {
    solution.fields[name] = value

    return updateSolution(auth, uuid, solution)
  }

  return null
}

//export async function deleteSolution(uuid) {}

export function buildSolution(fields) {
  /* 
    Format:

    {
      comments: [],
      contributors: [""],
      additionalContributor: ""
      description: "",
      documents: [{uuid: uuid}],
      text: [""],
      links: [],
      costValue: 0,
      costIsoCode: "",
      feedback: [],
      inventors: [""],
      name: "",
      hours: 0,
      isRequest: false,
      updateMessage: "",
    }
  */

  let completeFields = {}

  if (fields.comments) completeFields['comments'] = fields.comments

  if (fields.contributors && fields.additionalContributor) {
    completeFields['contributors'] = [
      ...fields.contributors,
      fields.additionalContributor
    ]
  } else if (fields.contributors) {
    completeFields['contributors'] = fields.contributors
  } else if (fields.additionalContributor) {
    completeFields['contributors'] = [fields.additionalContributor]
  }

  if (fields.description) completeFields['description'] = fields.description
  if (fields.documents) completeFields['documents'] = fields.documents

  if (fields.costValue && fields.costIsoCode) {
    completeFields['estimatedCost'] = {
      fields: {
        costValue: parseFloat(fields.costValue),
        isoCode: fields.costIsoCode
      },

      microschema: {
        name: 'cost'
      }
    }
  }

  if (fields.feedback) completeFields['feedback'] = fields.feedback
  if (fields.inventors) completeFields['inventors'] = fields.inventors
  if (fields.hours) completeFields['hours'] = parseInt(fields.hours)
  if (fields.links) completeFields['links'] = fields.links

  fields.isRequest
    ? (completeFields['isRequest'] = fields.isRequest)
    : (completeFields['isRequest'] = false)

  fields.name && fields.name !== ''
    ? (completeFields['name'] = fields.name)
    : (completeFields['name'] = 'solution-' + new Date().toISOString())

  if (fields.skills) completeFields['skills'] = fields.skills

  fields.slug
    ? (completeFields['slug'] = fields.slug)
    : (completeFields['slug'] = slugify(completeFields['name']))

  if (fields.text) completeFields['text'] = fields.text

  if (fields.updateMessage)
    completeFields['updateMessage'] = fields.updateMessage

  return {
    language: 'en',
    fields: completeFields,
    schema: {
      name: 'solution'
    }
  }
}
