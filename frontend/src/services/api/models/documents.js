import { graphQl, get_file, post, upload_file } from '../api'
import { publishNode } from './nodes'

export async function getDocumentDetails(auth, uuid) {
  return graphQl(
    auth,
    `{
    node(uuid: "${uuid}") {
      ... on documents {
        fields {
          binary {
            binaryUuid
            fileName
            mimeType
          }
        }
      } 
    }
  }`
  )
}

export async function downloadDocument(auth, uuid, fieldName) {
  return get_file(auth, `/solutions/nodes/${uuid}/binary/${fieldName}`)
}

export async function uploadDocument(auth, name, binaryData) {
  // Create document node
  const data = await post(auth, `/solutions/webroot/documents/${name}`, {
    language: 'en',
    schema: {
      name: 'documents'
    },
    fields: {
      name: name
    }
  })

  if (!data.uuid) {
    return
  }

  return upload_file(
    auth,
    `/solutions/nodes/${data.uuid}/binary/binary`,
    binaryData,
    name,
    name,
    data.version,
    data.language
  ).then((value) => {
    publishNode(data.uuid, 'en')
    return { uuid: value.uuid }
  })
}

export async function newPost(auth, category, subcategory, text) {
  if (subcategory === undefined) {
    subcategory = ''
  } else {
    subcategory = '/' + subcategory
  }

  var id = Math.random().toString(36).substring(2, 15)

  return post(auth, `/solutions/webroot/${category}${subcategory}/${id}`, {
    language: 'en',
    schema: {
      name: 'comment'
    },
    fields: {
      id: id,
      value: text
    }
  }).then((data) => {
    return { uuid: data.uuid, language: data.language }
  })
}
