import { get, post } from '../api'

export async function getRating(auth, uuid) {
  const solution = await get(auth, `/solutions/nodes/${uuid}`)
  if (solution.uuid) {
    return {
      data: await get(
        auth,
        `/solutions/plugins/ratings/${solution.fields.ratings.uuid}`
      ),
      uuid: solution.fields.ratings.uuid
    }
  }
}

export async function submitRating(auth, uuid, rating) {
  return post(auth, `/solutions/plugins/ratings/${uuid}/${rating}`)
}

export async function createRating(auth, parent) {
  return post(auth, `/solutions/nodes`, {
    parentNode: { uuid: parent },
    language: 'en',
    schema: {
      name: 'ratings'
    },
    fields: {}
  }).then((data) => {
    if (!data) {
      throw new Error('failure')
    } else {
      return { uuid: data.uuid, language: data.language }
    }
  })
}
