# health-module

> OneStep Health Module

[![NPM](https://img.shields.io/npm/v/health-module.svg)](https://www.npmjs.com/package/health-module) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save health-module
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'health-module'
import 'health-module/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [mdeforest](https://github.com/mdeforest)
